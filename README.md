# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Curling Forum]
* Key functions (add/delete)
    1. [Membership Mechanism(Sign up/Sign in)]
    2. [Database read/write]
    3. [RWD]
    4. [user page]
    5. [post page]
    6. [post list page]
    7. [leave comment under any post]
    8. [create post]
* Other functions (add/delete)
    1. [Sign up/in with Google account]
    2. [CSS animation]
    3. [Change profile picture]
    4. [Security(html/javascript injection)]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|N(I use firebase host)|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
1. **RWD**: 使用Bootstrap，或使用```width: 100%```再用```margin: 0 auto```調到中間
2. **Log in/Sign up**: 使用firebase的```signInWithEmailAndPassword()```、```signInWithRedirect()```以及```createUserWithEmailAndPassword()```，當使用者log in時在Navigation Bar會顯示```Welcome, example@gmail.com!```並且所有按鈕都可以按，利用```classList.remove()```將disabled class去除並更改inner.HTML來顯示文字
3. **Log out**: 使用firebase的```signOut()```，log out時(不管在哪個html page)會跳出alert視窗表示登出成功，並disable在Navigation Bar New post、My post的按鈕且不顯示Welcome，disable的方式是利用```classList.add()```把按鈕加上Bootstrap的class```disabled```並在css設定```pointer-events: none```(沒有設定pointer-events的話還是可以點連結，只是沒有按鈕的效果)，不顯示Welcome是把inner.HTML改成""
4. **Database read/write**: 使用```firebase.database().ref().push()```和```firebase.database().ref().once('value')```。讀資料的方式和Lab06相同，只不過除了在post page的comment需要用child_added和second_count即時更新外，其他如顯示post list時不需要(因為我的New post、post page、post list page是用不同的html，所以每次點進去就用once就好)
5. **User Page**: 在這裡會顯示使用者的大頭貼、email和此使用者寫的所有文章(使用Bootstrap的table顯示)並可以更改大頭貼
6. **Post List Page**: 用Bootstrap的table顯示所有的文章
7. **Create Post Page**: 用```input type="text"```以及```textarea```讓使用者輸入標題和內容
8. **Post Page**: 因為post list page和post page是不同的html，所以使用query string傳遞post的pushID來得知要顯示哪篇文章，方式是在顯示list的時候就先把pushID寫在link裡面:```"<a href='postPage.html?postId=" + childSnapshot.key + "'>"```，在post page的時候再使用```var urlParams = new URLSearchParams(window.location.search)```和```var postID = urlParams.get('postId')```取得ID，就可以利用ID當作path來取得文章資料
9. **leave comment under any post**: 和Lab06做法相同
10. **Change profile picture**: 使用```input type="file"```讓使用者選擇圖片，偵測到change後用```firebase.storage().ref().child().push(file)```和```firebase.storage().ref().child().getDownloadURL()```取得圖片的url，再使用```user.updateProfile()```更改photoURL，完成後會跳出alert表示更改成功。若使用者沒有選擇大頭貼就一律顯示anonymous.png
11. **CSS animation**: 在進入論壇之前的index.html，利用CSS的```animation-name```、```animation-duration```以及```@keyframes fadein```來更改透明度讓背景圖片有fadein的感覺

## Security Report (Optional)
我想到的是HTML和Javascript Injection，若在```input file="text"```或者```textarea```沒有特別設定的話，使用者可以直接打入HTML或Javascript來更改網頁。預防的作法是利用```replace()```把所有的"<"/">"改成"&lt"/"&gt"，這樣就會把內容當成text來顯示，而不是當成程式碼