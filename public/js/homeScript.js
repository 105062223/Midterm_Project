function initApp(){
	var disNew = document.getElementById('disNew');
	var disMy = document.getElementById('disMy');
	var disOut = document.getElementById('disOut');
	var disWel = document.getElementById('disWel');
	
	var str_1 = "<tr><td>";
	var str_2 = "</td></tr>\n";
	
	disOut.addEventListener('click',function(){
		firebase.auth().signOut().then(function(){
            window.alert("Log out success");
        }).catch(function(e){
            window.alert(e.message);
        });
	});
	
	firebase.auth().onAuthStateChanged(function(user){
		if (user) {
			console.log(user);
			disNew.classList.remove('disabled');
			disMy.classList.remove('disabled');
			disOut.classList.remove('disabled');
			disWel.innerHTML = "Welcome, " + user.email + " !";
		} else {
			console.log('not logged in');
			disNew.classList.add('disabled');
			disMy.classList.add('disabled');
			disOut.classList.add('disabled');
			disWel.innerHTML = "";
		}
	});
		
	var postRef = firebase.database().ref('/post_list');
	var total_post = [];
		
	postRef.once('value').then(function(snapshot){
		snapshot.forEach(function(childSnapshot){
			var childData = childSnapshot.val();
			total_post[total_post.length] = str_1 + "<a href='postPage.html?postId=" + childSnapshot.key + "'>" + childData.post_title + "</a></td><td>" + childData.post_email + str_2;
		});
		document.getElementById('allposts').innerHTML = total_post.join('');
	}).catch(e => console.log(e.message));
}

window.onload = function () {
    initApp();
};