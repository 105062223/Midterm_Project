function initApp(){
	var userEmail = document.getElementById('userEmail');
	var disOut = document.getElementById('disOut');
	var disWel = document.getElementById('disWel');
	
	var user_uid = "";
	
	var str_1 = "<tr><td>";
	var str_2 = "</td></tr>\n";
	
	var profilePic = document.getElementById('profilePic');
	var uploadPic = document.getElementById('uploadPic');
	
	disOut.addEventListener('click',function(){
		firebase.auth().signOut().then(function(){
            
        }).catch(function(e){
            window.alert(e.message);
        });
	});
	
	firebase.auth().onAuthStateChanged(function(user){
		if (user) {
			console.log(user);
			user_uid = user.uid;
			userEmail.innerHTML = user.email;
			disWel.innerHTML = "Welcome, " + user.email + " !";
			
			/*list user posts*/
			var postRef = firebase.database().ref('/' + user_uid);
			var total_post = [];
	
			postRef.once('value').then(function(snapshot){
				snapshot.forEach(function(childSnapshot){
					var childData = childSnapshot.val();
					total_post[total_post.length] = str_1 + childData.post_title + str_2;
				});
				document.getElementById('myposts').innerHTML = total_post.join('');
			}).catch(e => console.log(e.message));
			
			/*upload profile pic*/
			uploadPic.addEventListener('change',function(evt){
				var file = this.files[0];
				firebase.storage().ref().child(user_uid + "/" + file.name).put(file).then(function(){
					console.log("uploaded to storage");
					firebase.storage().ref().child(user_uid + "/" + file.name).getDownloadURL().then(function(url){	
						user.updateProfile({photoURL: url}).then(function(){
							window.alert("Upload success");
						}).catch(function(e){
							console.log(e.message);
						});
					}).catch(function(e){
						console.log(e.message);
					});
				}).catch(function(e){
					console.log(e.message);
				});
			});
			
			/*profile pic*/
			if(user.photoURL===null){
				profilePic.src = "image/anonymous_big.png";
			}else{
				console.log(user.photoURL);
				profilePic.src = user.photoURL;
			}
		} else {
			console.log('not logged in');
			disWel.innerHTML = "";
			document.location.href = "home.html";
		}
	});
}

window.onload = function () {
    initApp();
};