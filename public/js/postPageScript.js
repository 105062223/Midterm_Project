function initApp(){
	var disNew = document.getElementById('disNew');
	var disMy = document.getElementById('disMy');
	var disOut = document.getElementById('disOut');
	var disWel = document.getElementById('disWel');
	
	var urlParams = new URLSearchParams(window.location.search);
	var postID = urlParams.get('postId');
	
	var postTitle = document.getElementById('postTitle');
	var postedBy = document.getElementById('postedBy');
	var postContent = document.getElementById('postContent');
	
	var comment = document.getElementById('comment');
	var comment_btn = document.getElementById('comment_btn');
	var comment_list = document.getElementById('comment_list');
	var str_1 = '<div class="panel panel-default"><div class="panel-body"><div class="media media-left"><img class="rounded"';
	var str_2 = '></div><div class="media-body"><h5 class="media-heading"><strong>';
	var str_3 = '</strong></h5><p>';
	var str_4 = '</p></div></div></div>';
	
	var total_comment = [];
	var first_count = 0;
	var second_count = 0;
	
	disOut.addEventListener('click',function(){
		firebase.auth().signOut().then(function(){
            window.alert("Log out success");
        }).catch(function(e){
            window.alert(e.message);
        });
	});
	
	firebase.auth().onAuthStateChanged(function(user){
		if (user) {
			console.log(user);
			disNew.classList.remove('disabled');
			disMy.classList.remove('disabled');
			disOut.classList.remove('disabled');
			disWel.innerHTML = "Welcome, " + user.email + " !";
			
			comment_btn.addEventListener('click',function(){
				if(comment.value!=''){
					var comment_value = comment.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
					if(user.photoURL===null){
						firebase.database().ref(postID + '/').push().set({
							photo_src: "image/anonymous_big.png",
							comment_email: user.email,
							comment_content: comment_value
						}).then(function(){
							console.log('Add comment success');
						}).catch(function(e){
							console.log(e.message);
						});
					}else{
						firebase.database().ref(postID + '/').push().set({
							photo_src: user.photoURL,
							comment_email: user.email,
							comment_content: comment_value
						}).then(function(){
							console.log('Add comment success');
						}).catch(function(e){
							console.log(e.message);
						});
					}
				}
				comment.value = "";
			});
		} else {
			console.log('not logged in');
			disNew.classList.add('disabled');
			disMy.classList.add('disabled');
			disOut.classList.add('disabled');
			disWel.innerHTML = "";
			
			comment_btn.addEventListener('click',function(){
				window.alert("Please log in first!");
				comment.value = "";
			});
		}
	});
	
	firebase.database().ref('post_list/').child(postID).once('value').then(function(snapshot){
		console.log(snapshot);
		postTitle.innerHTML = snapshot.val().post_title;
		postContent.innerHTML = snapshot.val().post_content;
		postedBy.innerHTML = "posted by " + snapshot.val().post_email;
	});
	
	firebase.database().ref(postID + '/').once('value').then(function(snapshot){
		snapshot.forEach(function(childSnapshot){
			var childData = childSnapshot.val();
			total_comment[total_comment.length] = str_1 + 'src="' + childData.photo_src + '"' + str_2 + childData.comment_email + str_3 + childData.comment_content + str_4;
			first_count += 1;
		});
		comment_list.innerHTML = total_comment.join('');
		
		firebase.database().ref(postID + '/').on('child_added',function(data){
			second_count += 1;
			if(second_count > first_count){
				var childData = data.val();
				total_comment[total_comment.length] = str_1 + 'src="' + childData.photo_src + '"' + str_2 + childData.comment_email + str_3 + childData.comment_content + str_4;
				comment_list.innerHTML = total_comment.join('');
			}
		});
	});
}

window.onload = function () {
    initApp();
};