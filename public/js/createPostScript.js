function initApp(){
	var disOut = document.getElementById('disOut');
	var disWel = document.getElementById('disWel');
	
	var title = document.getElementById('inputTitle');
	var content = document.getElementById('inputContent');
	var submit_btn = document.getElementById('submitPost');
	
	var user_email = "";
	var user_uid = "";
	
	disOut.addEventListener('click',function(){
		firebase.auth().signOut().then(function(){
            
        }).catch(function(e){
            window.alert(e.message);
        });
	});
	
	firebase.auth().onAuthStateChanged(function(user){
		if (user) {
			console.log(user);
			user_email = user.email;
			user_uid =  user.uid;
			disWel.innerHTML = "Welcome, " + user.email + " !";
		} else {
			console.log('not logged in');
			disWel.innerHTML = "";
			document.location.href = "home.html";
		}
	});
	
	submit_btn.addEventListener('click',function(){
		if(title.value != "" && content.value != ""){
			var title_value = title.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
			var content_value = content.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
			firebase.database().ref('/post_list/').push().set({
				post_email: user_email,
				post_title: title_value,
				post_content: content_value
			}).then(function(){
				create_alert("success","Create post success!");
				title.value = "";
				content.value = "";
			}).catch(function(e){
				create_alert("error", e.message);
			});
			
			firebase.database().ref('/' + user_uid).push().set({
				post_title: title_value
			}).then(function(){
				console.log("Create post under user success");
			}).catch(function(e){
				console.log(e.message);
			});
		}
	});
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade in' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade in' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};