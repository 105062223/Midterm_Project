function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btnGoogle');
    var btnSignUp = document.getElementById('btnSignup');
	
	var disNew = document.getElementById('disNew');
	var disMy = document.getElementById('disMy');
	var disOut = document.getElementById('disOut');
	var disWel = document.getElementById('disWel');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
            document.location.href = "home.html";
        }).catch(function(e){
            create_alert("error",e.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    var provider = new firebase.auth.GoogleAuthProvider();

    btnGoogle.addEventListener('click', function () {
        firebase.auth().signInWithRedirect(provider);
    });
	
	firebase.auth().getRedirectResult().then(function (result) {
		if (result.credential){
			var token = result.credential.accessToken;
		}
		var user = result.user;
	}).catch(function (error) {
		create_alert("error",error.message);
	});

    btnSignUp.addEventListener('click', function () {  
		var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
            create_alert("success","");
		}).catch(function(e){
            create_alert("error",e.message);
			txtEmail.value = "";
            txtPassword.value = "";
        });
    });
	
	disOut.addEventListener('click',function(){
		firebase.auth().signOut().then(function(){
            window.alert("Log out success");
        }).catch(function(e){
            window.alert(e.message);
        });
	});
	
	firebase.auth().onAuthStateChanged(function(user){
		if (user) {
			console.log(user);
			disNew.classList.remove('disabled');
			disMy.classList.remove('disabled');
			disOut.classList.remove('disabled');
			disWel.innerHTML = "Welcome, " + user.email + " !";
		} else {
			console.log('not logged in');
			disNew.classList.add('disabled');
			disMy.classList.add('disabled');
			disOut.classList.add('disabled');
			disWel.innerHTML = "";
		}
	});
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade in' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade in' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};